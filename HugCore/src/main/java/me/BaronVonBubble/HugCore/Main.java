package me.BaronVonBubble.HugCore;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;

public final class Main extends JavaPlugin {
	@Override
	public void onEnable() {
		getLogger().info("Hug has been enabled.");
	}
	@Override
	public void onDisable() {
		getLogger().info("Hug has been disabled.");
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		Player target = (Bukkit.getServer().getPlayer(args[0]));
		try{
		if(cmd.getName().equalsIgnoreCase("Hug")){
			if(args.length != 1){
				return false;
			}
			if(args.length > 1){
				return false;
			}
			if(args.length == 1){
				if(target == null){
					sender.sendMessage(args[0] + " is not online.");
				}
				if(args[0].equalsIgnoreCase(target.getName())){
						sender.sendMessage("You just hugged " + target.getName() +"! :D");
						target.sendMessage(sender.getName() + " just hugged you! :D");
						getConfig().set(sender.getName() + ".HugsGiven", getConfig().getInt(sender.getName() + ".HugsGiven") + 1);
						getConfig().set(target.getName() + ".HugsRecieved", getConfig().getInt(target.getName() + ".HugsRecieved") + 1);
						this.saveConfig();
					}
				return true;
				}
			return false;
			}
		if(cmd.getName().equalsIgnoreCase("Huggle")){
			if(args.length != 1){
				return false;
			}
			if(args.length > 1){
				return false;
			}
			if(args.length == 1){
				if(target == null){
					sender.sendMessage(args[0] + " is not online.");
				}
				else if(args[0].equalsIgnoreCase(target.getName())){
						sender.sendMessage("You just huggled " + target.getName() +"! :3");
						target.sendMessage(sender.getName() + " just huggled you! :3");
						getConfig().set(sender.getName() + ".HugglesGiven", getConfig().getInt(sender.getName() + "..HugglesGiven") + 1);
						getConfig().set(target.getName() + ".HugglesRecieved", getConfig().getInt(target.getName() + ".HugglesRecieved") + 1);
						this.saveConfig();
					}
				return true;
				}
			return false;
			}
		else if(cmd.getName().equalsIgnoreCase("Boop")){
			if(args.length != 1){
				return false;
			}
			if(args.length > 1){
				return false;
			}
			if(args.length == 1){
				if(target == null){
					sender.sendMessage(args[0] + " is not online.");
				}	
				if(args[0].equalsIgnoreCase(target.getName())){
					sender.sendMessage("You just booped " + target.getName() +"! ^-^");
					target.sendMessage(sender.getName() + " just booped you! ^-^");
					getConfig().set(sender.getName() + ".BoopsGiven", getConfig().getInt(sender.getName() + ".BoopsGiven") + 1);
					getConfig().set(target.getName() + ".BoopsRecieved", getConfig().getInt(target.getName() + ".BoopsRecieved") + 1);
					this.saveConfig();
					}
				return true;
				}
			return false;
			}
		else if(cmd.getName().equalsIgnoreCase("Glomp")){
			if(args.length != 1){
				return false;
			}
			if(args.length > 1){
			return false;
		}
			if(args.length == 1){
				if(target != null){
					sender.sendMessage(args[0] + " is not online.");
				}	
				if(args[0].equalsIgnoreCase(target.getName())){
					sender.sendMessage("You just glomped " + target.getName() +"! x3");
					target.sendMessage(sender.getName() + " just glomped you! x3");
					getConfig().set(sender.getName() + ".GlompsGiven", getConfig().getInt(sender.getName() + ".GlompsGiven") + 1);
					getConfig().set(target.getName() + ".GlompsRecieved", getConfig().getInt(target.getName() + ".GlompsRecieved") + 1);
					this.saveConfig();
					}
				return true;
				}
			return false;
			}
		else if(cmd.getName().equalsIgnoreCase("Kiss")){
			if(args.length != 1){
				return false;
			}
			if(args.length > 1){
				return false;
			}
			if(args.length == 1){
				if(target == null){
					sender.sendMessage(args[0] + " is not online.");
				}	
				else if(args[0].equalsIgnoreCase(target.getName())){
						sender.sendMessage("You just kissed " + target.getName() +"! <3");
						target.sendMessage(sender.getName() + " just kissed you! <3");
						getConfig().set(sender.getName() + ".KissesGiven", getConfig().getInt(sender.getName() + ".KissesGiven") + 1);
						getConfig().set(target.getName() + ".KissesRecieved", getConfig().getInt(target.getName() + ".KissesRecieved") + 1);
						this.saveConfig();
						}
				return true;
				}
			return false;
			}
		if(cmd.getName().equalsIgnoreCase("GetHugs")){
			if(args.length != 1){
				return false;
			}
			if(args.length > 1){
				return false;
			}
			    if(getConfig().getString(args[0] + ".HugsGiven") != null){
				    sender.sendMessage("Hugs Given: " + getConfig().getString(args[0] + ".HugsGiven") + ".");
				    sender.sendMessage("Hugs Recieved: " + getConfig().getString(args[0] + ".HugsRecieved") + ".");
			        return true;
		    }
		}
		else if(cmd.getName().equalsIgnoreCase("GetHuggles")){
			if(args.length != 1){
				return false;
			}
			if(args.length > 1){
				return false;
			}
			if(args.length == 1){
			    if(getConfig().getString(args[0] + ".HugglesGiven") != null){
				    sender.sendMessage("Huggles given: " + getConfig().getString(args[0] + ".HugglesGiven") + ".");
				    sender.sendMessage("Huggles recieved: " + getConfig().getString(args[0] + ".HugglesRecieved") + ".");
			        return true;
			    }
		    }
		}
		else if(cmd.getName().equalsIgnoreCase("GetBoops")){
			if(args.length != 1){
				return false;
			}
			if(args.length > 1){
				return false;
			}
			if(args.length == 1){
			    if(getConfig().getString(args[0] + ".BoopsGiven") != null){
				    sender.sendMessage("Boops given: " + getConfig().getString(args[0] + ".BoopsGiven") + ".");
				    sender.sendMessage("Boops recieved: " + getConfig().getString(args[0] + ".BoopsRecieved") + ".");
			        return true;
			    }
		    }
		}
		else if(cmd.getName().equalsIgnoreCase("GetGlomps")){
			if(args.length != 1){
				return false;
			}
			if(args.length > 1){
				return false;
			}
			if(args.length == 1){
			    if(getConfig().getString(args[0] + ".GlompsGiven") != null){
				    sender.sendMessage("Glomps given: " + getConfig().getString(args[0] + ".GlompsGiven") + ".");
				    sender.sendMessage("Glomps recieved: " + getConfig().getString(args[0] + ".GlompsRecieved") + ".");
			        return true;
			    }
		    }
		}
		else if(cmd.getName().equalsIgnoreCase("GetKisses")){
			if(args.length != 1){
			return false;
		}
			if(args.length > 1){
				return false;
			}
			if(args.length == 1){
			    if(getConfig().getString(args[0] + ".KissesGiven") != null){
				    sender.sendMessage("Kisses given: " + getConfig().getString(args[0] + ".KissesGiven") + ".");
				    sender.sendMessage("Kisses recieved: " + getConfig().getString(args[0] + ".KissesRecieved") + ".");
			        return true;
			    }
		    }
		}
		} catch(ArrayIndexOutOfBoundsException e){
			sender.sendMessage("Invalid number of arguments!");
		}
		return false;
	}
}


